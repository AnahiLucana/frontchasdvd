import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ProductosService} from "../services/productos.service";

@Component({
  selector: 'app-modificar-producto',
  templateUrl: './modificar-producto.component.html',
  styleUrls: ['./modificar-producto.component.css']
})
export class ModificarProductoComponent implements OnInit {
  idproductos:any;
  nombre_pro:any;
  descripcion:any;
  precio:any;
  constructor(private service: ProductosService, private router: Router) { }

  ngOnInit() {
  }
  modificarProdu(event) {
    event.preventDefault();
    const producto = {
      idproductos:this.idproductos,
      nombre_pro:this.nombre_pro,
    descripcion:this.descripcion,
    precio:this.precio

    };
     this.service.putProducto(producto,this.idproductos).subscribe((response) => {
    // const res = JSON.parse(response.text());
       console.log(response.text());


    });
  }

}

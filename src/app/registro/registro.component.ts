import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  nombre: any;
  correo: any;
  contrasena: any;
  factura: any;
  celular: any;
  nit: any;

  constructor(private service: UsuarioService) {
  }

  ngOnInit() {
    // this.cargar();
  }

  save(event) {
    event.preventDefault();
    const usuario = {
      nombre: this.nombre,
      correo: this.correo,
      contrasena: this.contrasena,
      factura: this.factura,
      celular: this.celular,
      nit: this.nit

    };
    this.service.postUsuario(usuario).subscribe((response) => {
      console.log(response.text());
      // const res = JSON.parse(response.text());





    });
  }


}

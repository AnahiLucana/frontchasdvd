import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarComprasComponent } from './actualizar-compras.component';

describe('ActualizarComprasComponent', () => {
  let component: ActualizarComprasComponent;
  let fixture: ComponentFixture<ActualizarComprasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarComprasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarComprasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ComprasService} from "../services/compras.service";

@Component({
  selector: 'app-actualizar-compras',
  templateUrl: './actualizar-compras.component.html',
  styleUrls: ['./actualizar-compras.component.css']
})
export class ActualizarComprasComponent implements OnInit {
  idproductos: any;
  idcompra: any;
  cantidad: any;

  constructor(private service: ComprasService, private router: Router) {
  }

  ngOnInit() {
  }

  actuCompras() {
   // event.preventDefault();
    const compra = {
      idcompra: this.idcompra,
      idproductos: this.idproductos,
      cantidad:this.cantidad

    };
    this.service.putCompra(compra,this.idcompra).subscribe((response) => {
      console.log(response.text());
      // const res = JSON.parse(response.text());


    });

  }
}

import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";
import {Router} from "@angular/router";
import {ProductosComponent} from "../productos/productos.component";
import {ProductosService} from "../services/productos.service";

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent implements OnInit {
  idproductos: any;
  nombre_pro: any;
  descripcion: any;
  precio: any;

  constructor(private service: ProductosService, private router: Router) {
  }

  ngOnInit() {
  }

  crearProdu(event) {
    event.preventDefault();
    const producto = {
      idproductos: this.idproductos,
      nombre_pro: this.nombre_pro,
      descripcion: this.descripcion,
      precio: this.precio


    };
    this.service.postProducto(producto).subscribe((response) => {
      console.log(response.text());
      // const res = JSON.parse(response.text());


    });

  }
}

import { Component, OnInit } from '@angular/core';
import {ProductosService} from "../services/productos.service";

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {

  data:any = [];

  constructor(private Productos: ProductosService) { }

  ngOnInit() {

    this.Productos.getProducto().subscribe(res => {
      this.data = JSON.parse(res['_body'])
      console.log(this.data);
    })

  }

}

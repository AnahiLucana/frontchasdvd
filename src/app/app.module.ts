import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { RegistroComponent } from './registro/registro.component';
import { LoginComponent } from './login/login.component';
import { InicioComponent } from './inicio/inicio.component';
import { ProductosComponent } from './productos/productos.component';
import { ListaProductosComponent } from './lista-productos/lista-productos.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { CrearProductoComponent } from './crear-producto/crear-producto.component';
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {UsuarioService} from "./services/usuario.service";
import {ProductosService} from "./services/productos.service";
import { EliminarProductoComponent } from './eliminar-producto/eliminar-producto.component';
import { ModificarProductoComponent } from './modificar-producto/modificar-producto.component';
import { ModificarUsuarioComponent } from './modificar-usuario/modificar-usuario.component';
import { EliminarUsuarioComponent } from './eliminar-usuario/eliminar-usuario.component';
import { ComprasComponent } from './compras/compras.component';
import { ActualizarComprasComponent } from './actualizar-compras/actualizar-compras.component';
import {ComprasService} from "./services/compras.service";

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    LoginComponent,
    InicioComponent,
    ProductosComponent,
    ListaProductosComponent,
    ListaUsuariosComponent,
    CrearProductoComponent,
    EliminarProductoComponent,
    ModificarProductoComponent,
    ModificarUsuarioComponent,
    EliminarUsuarioComponent,
    ComprasComponent,
    ActualizarComprasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
        {
          path: 'login',
          component: LoginComponent
        },
      {
        path: 'registro',
        component: RegistroComponent
      },
      {
        path: 'inicio',
        component: InicioComponent
      },
      {
        path: 'productos',
        component: ProductosComponent
      },
     {
        path: 'lista-usuarios',
        component: ListaUsuariosComponent
      },
      {
        path: 'crear-producto',
        component: CrearProductoComponent
      },
      {
        path: 'eliminar-producto',
        component: EliminarProductoComponent
      },
      {
        path: 'modificar-producto',
        component: ModificarProductoComponent
      },
      {
        path: 'modificar-usuario',
        component: ModificarUsuarioComponent
      },
      {
        path: 'eliminar-usuario',
        component: EliminarUsuarioComponent
      },
      {
        path: 'lista-productos',
        component: ListaProductosComponent
      },
      {
        path: 'compras',
        component: ComprasComponent
      },
      {
        path: 'actualizar-compras',
        component: ActualizarComprasComponent
      }

    ])
  ],
  providers: [UsuarioService, ProductosService, ComprasService],
  bootstrap: [AppComponent]
})
export class AppModule { }

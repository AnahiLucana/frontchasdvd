import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-modificar-usuario',
  templateUrl: './modificar-usuario.component.html',
  styleUrls: ['./modificar-usuario.component.css']
})
export class ModificarUsuarioComponent implements OnInit {
  correo:any
  ;
  nombre:any;
  contrasena:any;
  factura:any;
  celular:any;
  nit:any;
  constructor(private service:UsuarioService, private router:Router) { }

  ngOnInit() {
  }
  modificarUsuario(event) {
    event.preventDefault();
    const Usuario = {
      correo:this.correo,
      nombre:this.nombre,
      contrasena:this.contrasena,
      factura:this.factura,
      celular:this.celular,
      nit:this.nit


    };
    this.service.putUsuario(Usuario,this.correo).subscribe((response) => {
      //const res = JSON.parse(response.text());
      console.log(response.text());

    });
  }
}

import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {ProductosService} from "../services/productos.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  correo: any;
  contrasena: any;


  constructor(private service: UsuarioService, private router: Router) {

  }


  ngOnInit() {
  }

  registrar(event) {
    event.preventDefault();
    const usuario = {
      correo: this.correo,
      contrasena: this.contrasena

    };
    //this.service.postlogUsuario(usuario).subscribe((response) => {
      //console.log(response.text());
      //this.irProductos();


    //})
  }

  irProductos() {
    return this.router.navigate(["/", "productos"]);
  }

}

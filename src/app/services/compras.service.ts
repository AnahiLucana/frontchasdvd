import { Injectable } from '@angular/core';
import {Headers, Http, RequestMethod, RequestOptions} from '@angular/http';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from "rxjs/internal/Observable";
@Injectable()
export class ComprasService {
  constructor(private http: Http) {
  }

  postCompra(Compra) {
    let url = 'http://localhost:9085/UPB/compra/post';
    let data = JSON.stringify(Compra);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.post(url, data, requestOptions);
  }

  putCompra(Compra,idcompra) {
    let url = 'http://localhost:9085/UPB/compra/PUT/'+idcompra;
    let data = JSON.stringify(Compra);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Put,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.put(url, data, requestOptions);
  }

}

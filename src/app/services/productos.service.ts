import { Injectable } from '@angular/core';
import {Headers, Http, RequestMethod, RequestOptions} from '@angular/http';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from "rxjs/internal/Observable";
@Injectable()
export class ProductosService {
  constructor(private http: Http) {
  }

  getProducto() {
    let url = 'http://localhost:9085/UPB/productos/GET';
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Get,
      url: url,
      headers: headers,
    });

    return this.http.get(url, requestOptions);
  }

  postProducto(Producto) {
    let url = 'http://localhost:9085/UPB/productos/post';
    let data = JSON.stringify(Producto);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.post(url, data, requestOptions);
  }

  putProducto(Producto,idproductos) {
    let url = 'http://localhost:9085/UPB/productos/PUT/'+idproductos;
    let data = JSON.stringify(Producto);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Put,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.put(url, data, requestOptions);
  }

  deleteProdu(IDProductos) {
    let url = 'http://localhost:9085/UPB/productos/borrar/'+IDProductos;
    return this.http.delete(url);
  }
}

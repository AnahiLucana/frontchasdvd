import { Injectable } from '@angular/core';
import {Headers, Http, RequestMethod, RequestOptions} from '@angular/http';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from "rxjs/internal/Observable";
@Injectable()
export class UsuarioService {
  constructor(
    private http: Http
  ) {
  }

 getUsuario() {
    let url = 'http://localhost:9085/UPB/usuario/GET';
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Get,
      url: url,
      headers: headers,
   });

    return this.http.get(url, requestOptions);
  }
*
  postlogUsuario(usuario) {
    let url = 'http://localhost:9085/UPB/usuario/login';
    let data = JSON.stringify(usuario);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.post(url, data, requestOptions);
  }
  postUsuario(usuario) {
    let url = 'http://localhost:9085/UPB/usuario/post';
    let data = JSON.stringify(usuario);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.post(url, data, requestOptions);
   }

  putUsuario(Usuario,correo) {
    let url = 'http://localhost:9085/UPB/usuario/PUT/'+correo;
    let data = JSON.stringify(Usuario);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let requestOptions = new RequestOptions({
      method: RequestMethod.Put,
      url: url,
      headers: headers,
      body: data
    });

    return this.http.put(url, data, requestOptions);
  }


  deletesuario(Correo) {
    let url = 'http://localhost:9085/UPB/usuario/borrar/'+ Correo;
    return this.http.delete(url);
  }

}

import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {

  data:any = [];

  constructor(private Usuario: UsuarioService) { }

  ngOnInit() {

    this.Usuario.getUsuario().subscribe(res => {
      this.data = JSON.parse(res['_body'])
      console.log(this.data);
    })

  }

}

import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ComprasService} from "../services/compras.service";
import {ProductosService} from "../services/productos.service";

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})


export class ComprasComponent implements OnInit {
  idproductos: any;
  idcompra: any;
  cantidad: any;

  constructor(private service: ComprasService, private router: Router) {
  }

  ngOnInit() {
  }

  crearCompras(event) {
    event.preventDefault();
    const compra = {
      idcompra: this.idcompra,
      idproductos: this.idproductos,
      cantidad:this.cantidad

    };
    this.service.postCompra(compra).subscribe((response) => {
      console.log(response.text());
      // const res = JSON.parse(response.text());


    });

  }
}

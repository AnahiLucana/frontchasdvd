import { Component, OnInit } from '@angular/core';
import {UsuarioService} from "../services/usuario.service";
import {Router} from "@angular/router";
import {ProductosService} from "../services/productos.service";

@Component({
  selector: 'app-eliminar-producto',
  templateUrl: './eliminar-producto.component.html',
  styleUrls: ['./eliminar-producto.component.css']
})
export class EliminarProductoComponent implements OnInit {
  idproductos:any;

  constructor(private service:ProductosService, private router:Router ) { }

  ngOnInit() {
  }
  eliminarProdu(event) {
    event.preventDefault();

    this.service.deleteProdu(this.idproductos).subscribe((response) => {
      //const res = JSON.parse(response.text());
      console.log(response.text());


    });
  }

}
